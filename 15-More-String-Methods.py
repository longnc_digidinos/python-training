name = "Vu Quoc Tuan"
print(name.find("Tuan"))
print(name.find("Teo"))

chuoi = "Python la ngon ngu lap trinh"
print(chuoi.find("Python"))
print(chuoi.find("Python",5))
print(chuoi.find("Python",5,25))

print("Teo" in chuoi)
print("trinh" in chuoi)

print(chuoi.index("ngon ngu"))
print(chuoi.replace("Python", "PHP"))
print(chuoi.replace("Python", "PHP", 10)) # khong chay duoc


s = "Teo"
print(s.isalnum())
s = "Teo 123"
print(s.isalnum())
s = 'Teo$'
print(s.isalnum())

s = "Teo"
print(s.isalpha())
s = "Teo 123"
print(s.isalpha())
s = "Teo$"
print(s.isalpha())

s = "123"
print(s.isdigit())
s = "abc"
print(s.isdigit())

s = "Vu Quoc Tuan"
print(s.isspace())
s = "     "
print(s.isspace())