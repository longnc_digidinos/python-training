print([c for c in "QHONLINE"])

print([i * j for i in ('x', 'y', 'z') for j in range(1,5)])

print([i*j*k for i in range(1,3) for j in range(1,3) for k in ('x','y','z')])

print([[x + y] for y in range(1,4) for x in range(1,4)])

print([[x + y for y in range(1,5)] for x in range(1,5)])

print([(x,y) for y in range(1,4) for x in range(1,4)])
