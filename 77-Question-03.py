def isprime(n) :
	if (n <= 0) :
		return False
	i = 2
	while i <= n//2 :
		if n % i == 0 :
			return False
		i += 1
	return True
print(isprime(74))