n1 = 900
print(type(n1))
n1 = 1000000000000000000000000000000000000
print(type(n1))

s = 2 ** 2
print(s)
# s = 2 *** 2
s = s + 1
print(s)
s1 = 5 * s
print(s1)

f = 9.5
print(type(f))
f = 9.536467737563757537373
print(f)
print(type(f))

c1 = 80
c2 = 115.5
c = c1 + c2
print(c)
print(type(c), id(c))

a = 10
b = 5
tong = a + b
print(tong)
hieu = a - b
print(hieu)
tich = a * b
print(tich)
thuong = a / b
print(thuong)
print(type(thuong))
thuong = b // a
print(thuong)
laydu = b % a
print(laydu)
