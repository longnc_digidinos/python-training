import numpy as np

A = np.arange(25).reshape(5,5)

print(A[0])
print(A[2][2])
print(A[2:4, 1:4])
print(A[[1,2,4]])
print(A.sum())
B = A[1,2,4]
print(B.sum())