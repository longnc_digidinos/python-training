x = 10
x = x + 5
print(x)

x = 10
x += 5
print(x)

x = 10
print(x + 1)

x = 10
# print(x++)
x += 1
print(x)

x = 10
x = 10 - 5
print(x)


x = 10
x -= 5
print(x)

x = 5
x *= 5
print(x)

x = 25
x /= 5
print(x)

x = 10
x = x ** 2
print(x)

x = 10
x **= 2
print(x)

x = 10
x = x / 3
print(x)

x = 10
x //= 3
print(x)

x = 5
x = x * 3
x = x * 3
print(x)

x = 5
x = x << 3
print(x)

x = 3
print(x)
x <<= 3
print(x)

x = 4
x <<= 2
print(x)

x = 5
x <<= 2
print(x)

x = 27
x >>= 3
print(x)

x = 64
x >>= 8
print(x)