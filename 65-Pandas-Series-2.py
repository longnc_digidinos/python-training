import numpy as np
import pandas as pd
from pandas import Series, DataFrame
from numpy.random import randn

diem = [10,9,10,7]
hocsinh = ["Teo", "Ti", "Tuan", "Tun"]
S3 = Series(diem, index = hocsinh)

s3d = S3.to_dict()
print(s3d)
S4 = Series(s3d)
print(S4)

print(pd.isnull(S4))
print(pd.notnull(S4))

S4.name = "Vu Quoc Tuan" #ten cua series
print(S4)