import sqlite3

db = sqlite3.connect("school.sql")
db.execute("DROP TABLE IF EXISTS student")
db.execute("CREATE TABLE student (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR NOT NULL)")

db.execute("INSERT INTO student(name) VALUES('Bui Quoc Huy')")
db.execute("INSERT INTO student(name) VALUES('Vu Quoc Tuan')")
db.execute("INSERT INTO student(name) VALUES('Tony Teo')")
db.execute("INSERT INTO student(name) VALUES('Jackson')")
db.commit()

results = db.execute("SELECT * FROM student")
for row in results :
	print(row)
print("-" * 20)

results = db.execute("SELECT * FROM student WHERE id = 2")
for row in results :
	print(row)
print("-" * 20)

db.execute("UPDATE student SET name = 'Conan Vu' WHERE id = 4")
results = db.execute("SELECT * FROM student WHERE id = 4")
for row in results :
	print(row)
print("-" * 20)

db.execute("DELETE FROM student WHERE id = 4")
results = db.execute("SELECT * FROM student")
for row in results :
	print(row)