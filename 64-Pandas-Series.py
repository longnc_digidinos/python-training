import numpy as np
import pandas as pd
from pandas import Series, DataFrame
from numpy.random import randn

S1 = Series([10,20,30,40])
print(S1)
S2 = Series([10,20,30,40], index = ['A', 'B', 'C', 'D'])
print(S2)

diem = [10,9,10,7]
hocsinh = ["Teo", "Ti", "Tuan", "Tun"]

S3 = Series(diem, index = hocsinh)
print(S3)
print(S3['Tuan'])