import numpy as np
import pandas as pd
from pandas import Series, DataFrame
from numpy.random import randn

diem = [10, 8, 6, 9]
sinhvien = ["Teo", "Ti", "Tun", "Tuan"]

data = {'diem' : diem, 'sinhvien' : ['Teo', 'Ti', 'Tun', 'Tuan']}
df2 = DataFrame(data)
print(df2)

ind = "A B C D E F".split()
cols = "col1 col2 col3 col4 col5 col6".split()
x = []
for i in range(36) :
	x.append(np.random.randint(1,100))

x = np.array(x)
x = x.reshape(6,6)

df3 = DataFrame(x, index = ind, columns = cols)
print(df3)

new_ind = "A B C D E F G H K".split()
df4 = df3.reindex(new_ind, fill_value = 0)
print(df4)

new_cols = "col1 col2 col3 col4 col5 col6 col7".split()
df5 = df4.reindex(columns = new_cols, fill_value = 0)
print(df5)