class Student :
	def info(self, name, age) :
		print("Name : ", name, "Age : ", age)
jackson = Student()
jackson.info("Robin Jackson", 18)

class Student :
	def __init__(self, name, age) :
		print("Name : ", name, "Age : ", age)
teo = Student("Tony Teo", 26)

class Student :
	def __init__(self, name, age) :
		print("Name : ", name, "Age : ", age)
	def __str__(self) :
		return ("---------------------------")
tuan = Student("Vu Quoc Tuan", 26)
print(tuan)

class Student :
	def __init__(self, name, age) :
		print("Name : ", name, "Age : ", age)
	def __str__(self) :
		return ("---------------------------")
	def __del__(self) :
		print ("destroyed")
tony = Student("Tony Teo", "26")
print(tony)
del tony