import numpy as np
import pandas as pd
from pandas import Series, DataFrame
from numpy.random import randn

diem = [10,9,10,7]
hocsinh = ["Teo", "Ti", "Tuan", "Tun"]
S3 = Series(diem, index = hocsinh)

S4 = S3 * 2
print(S4)
print(S4[2])
print(S4[['Ti', 'Tun']])
print(S4[S4 < 20])
