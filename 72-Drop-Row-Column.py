import numpy as np
import pandas as pd
from pandas import Series, DataFrame
from numpy.random import randn

df1 = pd.read_clipboard()
print(df1)

print(df1.drop('D'))

df1.drop('col4', axis = 1)

df6 = DataFrame(np.arange(4).reshape(2,2), columns = list('AB'), index = list('xy'))
print(df6)

df7 = DataFrame(np.arange(9).reshape(3,3), columns = list('ABC'), index = list('xyz'))
print(df7)

print(df6 + df7)

print(df6.add(df7, fill_value = 0))