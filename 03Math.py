print(min(3, 8, 1, 9, 15))

print(max(3, 8, 1, 9, 15))

print(abs(-115))
print(abs(115))

import math

print(math.sqrt(9))
print(math.factorial(5))
print(math.ceil(4.1))
print(math.floor(4.9))
print(10 < 8)
print(10 > 8)
