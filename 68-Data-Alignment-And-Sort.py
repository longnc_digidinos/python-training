import numpy as np
import pandas as pd
from pandas import Series, DataFrame
from numpy.random import randn

diem = [10,9,10,7]
hocsinh = ["Teo", "Ti", "Tuan", "Tun"]
S3 = Series(diem, index = hocsinh)

S8 = Series([0,1,2], index = ['A', 'B', 'C'])
S9 = Series([3,4,5,6], index = ['A', 'B', 'C', 'D'])
print(S8 + S9)

ind = "a b c d e f g h i j".split()
print(ind)

x = []
for i in range(10) :
	x.append(np.random.randint(1,100))
S10 = Series(x, index = ind)

print(S10.sort_values(ascending=True))