for letter in 'Python' :
	if letter != 'h' :
		print(letter)


for letter in 'Python' :
	if letter == 'h' :
		continue
	print(letter)

for letter in 'Python' :
	if letter == 'h' :
		break
	print(letter)

for letter in 'Python' :
	if letter == 'h' :
		pass
		print("passed")
	print(letter)