def myRange(*thamso) :
	start = stop = step = 0
	if (len(thamso) == 3) :
		start = thamso[0]
		stop = thamso[1]
		step = thamso[2]
	elif (len(thamso) == 2) :
		start = thamso[0]
		stop = thamso[1]
		step = 1
	else :
		start = 0
		stop = thamso[0]
		step = 1
	i = start
	while (i < stop) :
		yield i
		i = i + step
A = myRange(5)
print(list(A))
A = myRange(0,5)
print(list(A))
A = myRange(0,5,2)
print(list(A))