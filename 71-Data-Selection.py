import numpy as np
import pandas as pd
from pandas import Series, DataFrame
from numpy.random import randn

df1 = pd.read_clipboard()
print(df1)

print(df1['col1'])
print(df1['col5'])
print(df1[['col5', 'col6']])
print(df1[df1['col4'] > 50])
print(df1.ix['E'])