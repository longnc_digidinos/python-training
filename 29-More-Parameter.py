chuoi = "{:10} {:10}".format("Apple", 10000)
print(chuoi)

def dienthoai(**data) :
	totalprice = 0
	for name,price in data.items() :
		row = "{:20}{:10}".format(name,price)
		print(row)
		totalprice = totalprice + price
	return totalprice
total = dienthoai(iphone = 50000, nokia = 10000, samsung = 5500)
print("-" * 30)
print("{:20}{:10}".format("Total", total))