import numpy as np
import pandas as pd
from pandas import Series, DataFrame
from numpy.random import randn

diem = [10,9,10,7]
hocsinh = ["Teo", "Ti", "Tuan", "Tun"]
S3 = Series(diem, index = hocsinh)

print(S3)

print(S3.index)
print(S3.values)
print(S3[2])
print(S3[2:])

x = list(range(11,16))
ind = ['A', 'B', 'C', 'D', 'E']
S5 = Series(x, ind)
print(S5)

ind2 = ['A', 'B', 'C', 'D', 'E', 'F', 'G']
S6 = S5.reindex (ind2, fill_value = 0)
print(S6)