import numpy as np

mangA = np.array([1,2,3,4,5])

print(mangA)

mangB = np.arange(25)
print(mangB)
print(mangB.reshape(5,5))

print(np.arange(25).reshape(5,5))

abc = np.array([1,2,3,4],[5,6,7,8])
print(abc.shape)

print(np.zeros(5))
print(np.ones(5))