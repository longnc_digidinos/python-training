def myReduce(fn, C) :
	reduceVal = 0
	for x in C :
		reduceVal = fn(reduceVal, x)
	return reduceVal

A = [1,2,3,4,5]
print(myReduce(lambda x,y : x if x > y else y, A))

A = [1,2,3,4,5]
print(myReduce(lambda x,y : x + y, A))