class Person :
	def __init__ (self, name, age) :
		self.name = name
		self.age = age
	def info(self) :
		print("Toi ten la", self.name, " tuoi toi la ", self.age)
asia = Person("Vu Quoc Tuan", 26)
asia.info()

class Student :
	def __init__ (self, grade) :
		self.grade = grade

	def infoGrade(self) :
		print("Lop cua ban la ", self.grade)
hsa = Student(6/5)
hsa.infoGrade()

class Person :
	def __init__ (self, name, age) :
		self.name = name
		self.age = age
	def info(self) :
		print("Toi ten la", self.name, " tuoi toi la ", self.age)

class Student (Person):
	def __init__ (self, name, age, grade) :
		Person.__init__(self, name, age)
		self.grade = grade

	def infoGrade(self) :
		print("Lop cua ban la ", self.grade)
hsa = Student("Vu Quoc Tuan", 26, 6/5)
hsa.infoGrade()
hsa.info()