s = "Toi ten la {} va tuoi cua toi la {}".format("Vu quoc tuan", 26)
print(s)
hs = "Hoc sinh 1 : {2} hoc sinh 2 : {1} hoc sinh 3 : {0}".format("Teo", "Ti", "Tun")
print(hs)

ds = ["Com tam", "Bun bo", "Pizza"]
monan = "Cac mon an hien tai la {2}, {0}, {1}".format(*ds)
print(monan)

ds = ("Com tam", "Bun bo", "Pizza")
monan = "Cac mon an hien tai la {2}, {0}, {1}".format(*ds)
print(monan)

print('{:<20}'.format("ABCXYZ"))
print('{:-<20}'.format("ABCXYZ"))
print('{:->20}'.format("ABCXYZ"))
print('{:-^20}'.format("ABCXYZ"))
print('{:-^20}'.format("Quoctuan.info"))

s = "so thu tu : {}".format("12")
print(s)
s = "so thu tu : {0:d}".format(12)
print(s)
s = "so thu tu : {0:d} va {1:X}".format(12,25)
print(s)
s = "so thu tu : {0:d} va {1:b}".format(12,25)
print(s)
s = "so thu tu : {0:d} va {1:o}".format(12,25)
print(s)

price = 2000000000
print("{:,}".format(price))