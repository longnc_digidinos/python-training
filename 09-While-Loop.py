n = 1
while (n <= 10) :
	print(n, "Python")
	n = n + 1
	# n++

n = 1
while (n <= 100) :
	print(n , end = ' ')
	n += 1
print('\n')

n = 1
while (n <= 100) :
	if n % 2 == 0 :
		print(n , end = ' ')
	n += 1

n = 1
A = []
print(type(A))
A.append(10)
print(A)
A.append(30)
print(A)

n = 1
A = []
while (n <= 100) :
	if (n % 10 == 0) :
		A.append(n)
	n = n + 1
print(A)