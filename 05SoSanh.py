t1 = 15
t2 = 20
print(t1 < t2)
print(t1 > t2)
print(t1 <= t2)
print(t1 >= t2)

t1 = 15
t2 = 15
print(t1 <= t2)
print(t1 >= t2)
print(t1 == t2)
print(t1 != t2)

xx = 15.0
x = 15
print(xx == x)

print(type(xx), id(xx))
print(type(x), id(x))

print(x is xx)

x = 10
y = 10
print(type(x), id(x), type(y), id(y))
print(x is y)

A = [15, 5, 4, 21]
print(3 in A)
print(16 in A)

chuoi = "Toi ten Long"
print("Long" in chuoi)
print("Teo" in chuoi)