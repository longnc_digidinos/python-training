def myMap(fn, C) :
	newC = []
	for x in C :
		newC.append(fn(x))
	return newC
A = [1,2,3,4,5]
B = myMap(lambda y: y + 2, A)
print(B)
