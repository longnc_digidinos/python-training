import numpy as np
import pandas as pd
from pandas import Series, DataFrame
from numpy.random import randn

df1 = pd.read_clipboard()
print(df1)

print(df1.sum())
print(df1.sum(axis = 1))
print(df1.max())
print(df1.idxmax())
print(df1.cumsum())
print(df1.describe())

nd = np.nan
A = [1,2,3]
B = [4, nd, 6]
C = [nd, 8, nd]
df2 = DataFrame([A,B,C])
print(df2)
print(df2.dropna())
print(df2.dropna(how = 'all'))
print(df2.dropna(thresh = 2))
print(df2.fillna(0))
print(df2.fillna(0, inplace = True))
