import numpy as np

arr = np.array([[1,2,3,4], [5,6,7,8]])
print(arr)
print(arr + 10)
print(arr * arr)
print(arr ** 2)
print(arr - arr)
print("-" * 50)

A = np.arange(25).reshape(5,5)
print(A)
print(A.T)

B = np.arange(6).reshape(2,3)
print(B)
print(B.T)

