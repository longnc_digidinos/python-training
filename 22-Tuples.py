t = (1,2,3)
print(t[1])

t = t + t + t + t
print(t)

t = (1,2,3)
t = t * 4
print(t)
print(t[2:6])
print(t[-2])
print(t[-2:-6])
print(t[-6:-2])
print(t[3:])
print(t[:6])

t1 = (1,2,3)
t2 = (4,5,6)
t = (t1,t2)

l1 = ["Teo", "Tuan"]
t1 = ("Truc", "Trinh")
t = (l1,t1)
print(t)

t = ("Tuan", "Teo","Tai","Tu","Trinh")
print(t)
# t[2] = "Tun"

l = [1,2,3]
t = (5,6,7,8)
t1 = (1,2,3,4,t,l)
print(t1)
t1[5][1] = 10
print(t1)
