for i in range(5,10,1) :
	print(i)

print("-" * 20)

def myRange(start, stop, step) :
	i = start
	while(i < stop) :
		print(i)
		i += 1
myRange(5,10,1)

print("-" * 20)

def myRange(start, stop, step) :
	i = start
	while(i < stop) :
		yield i
		i += 1

x = myRange(5,10,1)
for i in x :
	print(i)
