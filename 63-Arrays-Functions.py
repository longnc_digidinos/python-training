import numpy as np

x = []
y = []
for i in range(10) :
	x.append(np.random.randint(1,10))
	y.append(np.random.randint(1,10))
print(x,y)

x = np.array(x)
y = np.array(y)
x.sort()
y.sort()

print(x)
print(y)

print(np.unique(y))

print(np.greater(x,y))

b = np.greater(x,y)
print(b.all())
print(b.any())
print(np.equal(x,y))
