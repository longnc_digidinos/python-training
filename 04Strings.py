s = 'Python'
print(s)

t = "I love 'Python 3'"
print(t)
t = "I love \"Python 3\""
print(t)

print("c:\folder\name")
print("c:\\folder\\name")

print("Welcome to Python Course \nMy Name is Long")

print("""Chao mung ban den voi
			
			khoa hoc Python online""")

k1 = "Tony"
k2 = "Teo"
print(k1 + k2)

h = "My name is Long "  * 10
print(h)