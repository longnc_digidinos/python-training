import numpy as np

a = [1,2,3,4]
print(type(a))
arr1 = np.array(a)
print(type(arr1))
print(arr1)

b = [5,6,7,8]
arr2 = np.array(b)
print(arr2)

c = [10,11,12,13]
arr3 = np.array(c)

tongArray = np.array([arr1, arr2, arr3])
print(tongArray.shape)

A = np.arange(10)
print(A)

B = np.arange(5, 50, 3)
print(B)

teo = np.array([1,2,3,4,5,6])
print(teo)