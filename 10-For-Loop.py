t = (1,2,3,4,5)

print(type(t))

for n in t :
	print(n, end = ' ')

t = (1,3,5,7,9)
i = 0
for n in t :
	print (i, end = " ")
	i = i + 1

t = (1,3,5,7,9)
for n in t :
	print ("Python course")

l = [10,20,30,40,50,60,70,80,90,100]
print(type(l))
for n in l :
	print(n, end = " ")

d = {'Tuan': 26, 'Huy': 30, 'An': 26}
print(type(d))
for s in d :
	print(s)
for key in d :
	print(d[key])

ss = "Vu Quoc Tuan"
for s in ss :
	if s != ' ' :
		print(s * 10)
