def isprime(n) :
	if (n <= 0) :
		return False
	i = 2
	while i <= n//2 :
		if n % i == 0 :
			return False
		i += 1
	return True

cnt = 0
for i in range(1,1000) :
	if isprime(i) :
		print ("{:<3}".format(str(i)), end = " ")
		cnt += 1
	if cnt == 10 :
		print("")
		cnt = 0