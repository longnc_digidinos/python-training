s = {1,2,3,4,5}
print(len(s))
# print(s[2])

a,b,c,d,e = s
print(c)

for item in s :
	print(item)

s1 = set('aaabbbbbcccdddeee')
print(s1)

s1 = set('eeeefffaaabbbbcfccddd')
print(s1)

print('c' in s1)
print('k' in s1)

A = {1,2,3,4,5}
B = {8,4,5,6,7}
C = A,B # tuple
print(C)
print(A - B) # hieu cua A va B
print(A | B) # hop cua A va B
print(A & B) # giao cua A va B
