x = [1,2,3,4,5]

x = x + [10]
print(x)

x.append(11)
print(x)

# x.append(3,4,5)

x.insert(3,9)
print(x)

x.pop(3)
print(x)

print(x.count(5))

x = [3,5,2,6,1]
x.sort()
print(x)

name = ["Teo", "Tung", "An", "Binh"]
name.sort()
print(name)

x.reverse()
print(x)

name.reverse()
print(name)

del name[0]
print(name)

del name[0:2]
print(name)




