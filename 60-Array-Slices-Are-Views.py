import numpy as np

A = np.arange(20)
a1 = A[:10]
print(a1)
a1[:] = 100
print(a1)
print(A)

print("-" * 20)

x = list(range(20))
print(x)
y = x[:10]
print(y)
y[0] = 100
print(y)
print(x)

print("-" * 20)

A = np.arange(20)
B = A.copy()
print(B)