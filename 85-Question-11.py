def myFilter(fn, C) :
	newC = []
	for x in C :
		if fn(x) :
			newC.append(x)
	return newC
A = [1,2,3,4,5]
print(myFilter(lambda x : x % 2 == 0, A))

A = [10,5 ,11,25,2]
print(myFilter(lambda x : x > 10, A))