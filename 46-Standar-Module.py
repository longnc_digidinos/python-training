import random as ra

print(ra.randint(1,100000))

x = list(range(25))
print(x)

ra.shuffle(x)
print(x)

import datetime as dt

t = dt.datetime.now()
print(t)

print(t.hour)
print(t.minute)
print(t.day)

import os as o

print(o.getcwd())
print(o.getenv('PATH'))