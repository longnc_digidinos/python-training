import numpy as np
import pandas as pd
from pandas import Series, DataFrame
from numpy.random import randn

ind1 = "A A A B B B".split()
ind2 = "a b c a b c".split()
cols1 = "C1 C2 C2 C3 C3".split()
cols2 = "col1 col2 col3 col4 col5".split()

x = []
for i in range(30) :
	x.append(np.random.randint(1,100))
x = np.array(x)
x = x.reshape(6,5)
print(x)

df1 = DataFrame(x, index = [ind1, ind2], columns = [cols1, cols2])
print(df1)

print(df1[['C1', 'C2']])
