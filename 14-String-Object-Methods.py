name = "Vu Quoc Tuan"
print(name.capitalize())
print(name.upper())
print(name.lower())
print(name.swapcase())
chuoi = "Python"
chuoi.ljust(10, "*")
chuoi.ljust(10, " ")
chuoi.rjust(10, "*")
chuoi.center(14, "*")

nhap = 'day la noi dung                     '
print(nhap.rstrip())
nhap = '             day la noi dung'
print(nhap.lstrip())
print(nhap.strip())