d = {"name" : "Vu Quoc Tuan", "age" : "26", "website" : "quoctuan.info"}
print(d)
print(len(d))
print(d["age"])
d["age"] = 28
print(d)

del d["age"]
print(d)
print("age" in d)
print("name" in d)
print("26" in d) # chi kiem tra key

d.clear()
print(d)

d = {"hs1":"Teo", "hs2":"Tuan", "hs3":"Tun", "hs4":"Tung"}
print(d)
print(d.popitem()) # xoa ngau nhien


