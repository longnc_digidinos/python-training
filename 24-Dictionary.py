std = {"hs1":"Tuan", "hs2":"Teo", "hs3":"Tung"}
print(std)

print(len(std))
print(std['hs2'])
# print(std['tun'])
# std = std + std
del std['hs2']
print(std)
print(std.keys())
print(std.values())
print(std.items())

i = std.items()
for item in i :
	print(item[1])

diem = dict(A = 10, B = 6, C = 5)
print(diem)
list(diem)

result = {x:x ** 2 for x in range(10)}
print(result)